#include <stdio.h>

int main()
{
    int j;

    for (int i = 1; i < 101; i+=2)
    {
        printf("%d ", i);
    }

    printf("\n");

    for (int i = 2; i < 101; i+=2)
    {
        printf("%d ", i);
    }

    printf("\n");
    
    for (int i = 2; i < 101; i++)
    {
        for (j = 2; j < i; j++)
        {
            if (i % j == 0)
            {
                break;
            }
        }
        
        if (i == j)
        {
            printf("%d ", i);
        }
    }

    return 0;
}
